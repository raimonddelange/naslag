------ WSL Ubuntu

sudo curl -LJO "https://gitlab-runner-downloads.s3.amazonaws.com/latest/deb/gitlab-runner_${arch}.deb"

sudo dpkg -i gitlab-runner_<arch>.deb

-- Update
sudo gitlab-runner stop

dpkg -i gitlab-runner_<arch>.deb

sudo curl -L --output /usr/local/bin/gitlab-runner "https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64"

sudo chmod +x /usr/local/bin/gitlab-runner

~/.gitlab-runner/
which gitlab-runner

-- Create a gitlab CI user

sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash

sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner

sudo gitlab-runner start

https://docs.gitlab.com/runner/commands/#gitlab-runner-unregister

-- Check gitlab-runner 
gitlab-runner list

gitlab-runner verify

-- Change config
C:\Users\Raimond\config.toml
or 
gitlab-runner register --non-interactive --name my-runner --url http://gitlab.example.com --registration-token my-registration-token
Enter the GitLab instance URL (for example, https://gitlab.com/):
[http://gitlab.example.com]: https://gitlab.ic.uva.nl/
Enter the registration token:
[my-registration-token]: VcYn9sAHnmxmkUyse5Xw
Enter a description for the runner:
[my-runner]: Infra-runner
Enter tags for the runner (comma-separated):
OH
Registering runner... succeeded                     runner=VcYn9sAH
Enter an executor: virtualbox, kubernetes, parallels, shell, ssh, docker+machine, docker-ssh+machine, custom, docker, docker-ssh:
shell
Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!

sudo vim /home/raimond/.gitlab-runner/config.toml
sudo vim /etc/gitlab-runner/config.toml

-- Start in non-interactive mode
sudo gitlab-runner start 

Start in interactive mode
sudo gitlab-runner run


