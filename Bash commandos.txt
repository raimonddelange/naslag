----Ubuntu update
sudo apt list --upgradable
sudo apt-get update
sudo apt-get upgrade
sudo apt full-upgrade
sudo reboot

Controleren waar de exe staat:
which gitlab-runner
ps aux
a = show processes for all users
u = display the process's user/owner
x = also show processes not attached to a terminal

ps -e
----------------------Permissions

https://www.redhat.com/sysadmin/manage-permissions

chown raimond:raimond file (wijzigen account en group van file)
ls	Inhoud huidige directory
ls -la	Show permissions
ls -a   Ook hidden
ls -l	Long list
sudo (Als superuser starten) 
sudo -s Root user
chmod +x file1 (ik krijg execute rechten)
chmod 700 (alleen ik rechten)
chmod 744 file1
chmod 644 file1 
chmod 755 file1 (ik alle rechten group en everyone alleen lees en exe rechten)
chmod +x update
./update
----------------------Terminal settings
When bash initializes a non-login interactive bash shell on a Debian/Ubuntu-like system, the shell first reads /etc/bash.bashrc and then reads ~/.bashrc

sudo vim ~/.bashrc (open settings interactive terminal) https://askubuntu.com/questions/540683/what-is-a-bashrc-file-and-what-does-it-do 
sudo vim bash.bashrc

----------------------Status
pwd	Huidige directory
file <filenaam> toont welke type file het is.
locate <filenaam> zoeken naar files
users (wie is ingelogd)
id (account gegevens)
which cal
cal calender
history (lijst met commando's)
whatis cal (
apropos time (
echo $PATH

----------------------Wijzigen path
cd of cd ~ (goto start directory)
cd ..
cd /inse<tab>
cd /usr/local/bin
cd /usr/local
cd /mnt/c/Users/Raimond
---------------------Bewerkingen
popd
pushd /etc
man man (Manual)
mkdir (directory maken)
touch (datum tijd) of als file niet bestaat deze aanmaken.
cp ~/.bashrc bashrc (kopy file vanuit root directory naar huidig directory) 
cp bashrc bashrc.bak
mv bashrc file1 (toevoegen bashrc aan file1).
rm (verwijderen file (zonder prullebak))
rm -r (verwijderen directories die vol zijn)
rmdir (verwijderen directories die leeg zijn)
cat file1 (list file1)
cat >> file2 (type iets in een file) (append)
cat > file1 nieuwe tekst
more file1 (list met spatie wacht)
Less file1 (door de file heenlopen).
history | less (pipe tekst van 1 cmd in een andere cmd).
> file.txt (tekst in een file).
mkdir (maak directory)
watch <opdracht> (herhalen van een opdracht)
killall firefox
ctrl l
Move directory
mv /path/sourcefolder/* /path/destinationfolder/
mv /mnt/c/Users/Raimond/onderwijshub-shared/*.txt /mnt/c/Users/Raimond/onderwijshub-shared/naslag/

-------------------Editors
-----------Nano
sudo nano file2 (file editor) ctrl o opslaan

-----------VIM
sudo vim ~/.bashrc
:q quit
:q! quit without saving
: save without quit
:w [newfilename] Rename file
:x save and quit

-----------Bash Shell scripting

-------------------------------------------------------------------
Windows verkenner lokatie
\\wsl$\Ubuntu







