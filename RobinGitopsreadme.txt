Onderwijshub  gitops
This git repository contains all the configuration needed to provision and use Argo CD.

Bootstrapping Argo CD

Preparation
Some of the files including the secrets aren't part of this git repository. Please make sure you obtain a copy of them before you continue. Also, make sure your current working directory matches
the bootstrap folder of the relevant environment.
cd bootstrap/<environment>

Installation
First of all, make sure you install Argo CD on your Kubernetes cluster:
kubectl create namespace argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml

Uninstallation
If something went wrong with the Argo cd installation:
kubectl delete -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
Remark:

Uninstallation hangs after:
networkpolicy.networking.k8s.io/argocd-application-controller-network-policy
Press CTRL ESC

Access the UI via port forwarding
Currently we might have a bit of a chicken-and-egg-problem where we need to configure Argo CD to become accessible via the nginx ingress server. But now what, we don't have nginx installed yet, and don't we need Argo CD for that?
So for now we will access Argo CD using port forwarding while setting up
the rest of the configuration. You can enable port forwarding using the following command:
kubectl port-forward svc/argocd-server -n argocd 8080:443
Now you should be able to login on localhost:8080. The username should be admin and you can retrieve the
password from the secret argocd-initial-admin-secret using the following cli-command:
kubectl get secret argocd-initial-admin-secret -n argocd -o jsonpath='{.data.password}' | base64 -d

Configuring the repositories
In order to bootstrap the cluster, Argo CD should be able to acccess the git repository containing the configuration and the relevant helm repositories/registries. Because we don't want the UI to become inaccessible, you have to disable all configuration keys in argocd-cm.yaml except for the repositories and repositories.credentials keys for now. After you have this reduced version of the configuration, apply it to your cluster:
kubectl apply -f secret.yaml
kubectl apply -f argocd-cm.yaml
In the UI (Settings -> Repositories) you should now see Argo CD being able to connect to the external sources it needs.

Create the applications from git
Now its time to install the applications on the cluster. Since all configuration is present in this repository and Argo CD is able to access this repository, most of it happens automagically once you tell it can find the configuration.
kubectl apply -f eventstore.yaml
Now you will see all the applications configured in this gitops repository ready for creation. Now you can run the synchronisation until you have all services running. 

You need a working ingress-nginx server in order to continue with the next step.

Allow access using the ingress server
This is the moment where we allow the public access to Argo CD. For this we need a few
config alterations. You can now re-enable the url key of the relevant argocd-cm.yaml
kubectl patch deployment argocd-server --patch "$(cat argocd-server-patch.yaml)"
kubectl apply -f ingress.yaml
kubectl apply -f argocd-cm.yaml
Now you should be able to access Argo CD using the configured url.

Enable authentication using Azure AD
Finally we can enable authentication using Azure AD by re-enabling all keys in the argocd-cm.yaml file and applying the final configuration:
kubectl apply -f argocd-rbac-cm.yaml
kubectl apply -f argocd-cm.yaml