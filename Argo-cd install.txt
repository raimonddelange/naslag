Aanname1: Azure resourcegroups zijn via Infra geinstalleerd.
Aanname2: 2 Public IP resources en Azure Keyvault zijn geinstalleerd.
Aanname3: Cname voor DNS record in Public IP adres is aangemaakt
Aanname4: SSL certificaat (public en private keys) van Cname is in Azure Keyvault ingevoerd.
Aanname5: De GITLAB runner draait onder een managed identity of een service principle die via Kubectl commando's read/write/delete acties kan uitvoeren (geregeld in de RBAC group RoleAssignmentContributor)
Aanname6: De GITLAB runner managed identity of service principle is in de policy van de Azure keyvault opgenomen zodat deze de secrets/certificates kan lezen.
Aanname7: Het IP adres van de GITLAB runner is bekend en is opgenomen in de IP configuratie van de Azure Keyvault en AKS.

---- Install Azure CLI on Ubuntu
NIET NODIG INDIEN VIA TERRAFORM
curl -sL https://aka.ms/InstallAzureCLIDeb | sudo bash

---- Login to tentant
NIET NODIG INDIEN VIA TERRAFORM
az login --tenant a0f1cacd-618c-4403-b945-76fb3d6874e5 (UvA)
az login --tenant 0907bb1e-21fc-476f-8843-02d09ceb59a7 (HvA)
Via Terraform
Of via managed-identity
https://docs.microsoft.com/en-us/azure/aks/use-managed-identity

---- Get credentials for AKS (set .kube config file).
NIET NODIG INDIEN VIA TERRAFORM
az aks get-credentials --name onderwijshub-eventstore-sbi-aks --resource-group onderwijshub-eventstore-sbi --subscription 277ff3e7-1e27-4859-9f05-c584c7a963ff

Of via managed-identity
https://docs.microsoft.com/en-us/azure/aks/use-managed-identity

--- Install Kubectl on Debian-based distributions
https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/

sudo apt-get update
sudo apt-get install -y apt-transport-https ca-certificates curl

sudo curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg

echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list

sudo apt-get update
sudo apt-get install -y kubectl

--- Set Kubenetes context
NIET NODIG INDIEN VIA TERRAFORM
https://kubernetes.io/docs/reference/access-authn-authz/authentication/
kubectl config use-context onderwijshub-uva-eventstore-sbi-aks

--- Controle Kubectl (indien nodig)
NIET NODIG INDIEN VIA TERRAFORM
kubectl get all --all-namespaces=true

Opm1: Let op dat de IP range waarmee verbonden wordt vanuit de Gitlab runner in het AKS netwerk configuratie op de whitelist staat.

Opm2: VPN moet uit staan om connectie te kunnen maken.

----- Install Argo-cd CLI (om Argo commando's te kunnen uitvoeren).
NIET NODIG INDIEN SYNC VIA SETTING IN YAML FILE
https://argo-cd.readthedocs.io/en/stable/cli_installation/

sudo curl -sSL -o /usr/local/bin/argocd https://github.com/argoproj/argo-cd/releases/latest/download/argocd-linux-amd64
sudo chmod +x /usr/local/bin/argocd

----- Install Argo-cd on AKS (om via Argo applicaties te kunnen deployen).
YAML FILE BEVAT GEEN OMGEVINGSSPECIFIEKE GEGEVENS.
kubectl create namespace argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml

----- Install Ingress-nginx controler (o.a. om Public IP adressen te routeren)
YAML FILE BEVAT GEEN OMGEVINGSSPECIFIEKE GEGEVENS.
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.0.5/deploy/static/provider/cloud/deploy.yaml

----- Install Azure AD POD identity applicatie in AKS om via management identity te kunnen werken.
YAML FILE BEVAT GEEN OMGEVINGSSPECIFIEKE GEGEVENS.
kubectl apply -f aad-pod-identity.yaml
autosync: 
argocd app sync -l app.kubernetes.io/instance=aad-pod-identity

----- Install Azure secrets store provider om de benodigde credentials uit de Azure keyvault kunnen gebruiken door de managent identity
YAML FILE BEVAT GEEN OMGEVINGSSPECIFIEKE GEGEVENS.
kubectl apply -f csi-secrets-store-provider-azure.yaml
autosync:
argocd app sync -l app.kubernetes.io/instance=csi-secrets-store-provider-azure

--------- VANAF HIER IS DE INSTALLATIE VOLGORDE en INHOUD YAML FILES ONDUIDELIJK>

----- Install oidc-azure-client-secret, Nexus and Gitlab credentials in AKS
GEGEVENS MOETEN UIT AZURE KEYVAULT WORDEN GEHAALD IPV UIT EEN SECRETS.YAML FILE.
kubectl apply -f secrets.yaml

----- Install Ingress-nginx application
YAML FILE BEVAT WEL OMGEVINGSSPECIFIEKE GEGEVENS.
kubectl apply -f nginx.yaml
argocd app sync -l app.kubernetes.io/instance=ingress-nginx

------ Allow access using the ingress server.
YAML FILE BEVAT GEEN OMGEVINGSSPECIFIEKE GEGEVENS.
kubectl patch deployment argocd-server --patch "$(cat argocd-server-patch.yaml)"
YAML FILE BEVAT WEL OMGEVINGSSPECIFIEKE GEGEVENS.
kubectl apply -f ingress.yaml
re-enable the url key of the relevant argocd-cm.yaml
YAML FILE BEVAT WEL OMGEVINGSSPECIFIEKE GEGEVENS.
kubectl apply -f argocd-cm.yaml
Now you should be able to access Argo CD using the configured url.

Finally we can enable authentication using Azure AD by re-enabling all keys in the argocd-cm.yaml file and applying the final configuration:
YAML FILE BEVAT WEL OMGEVINGSSPECIFIEKE GEGEVENS.
kubectl apply -f argocd-rbac-cm.yaml
YAML FILE BEVAT GEEN OMGEVINGSSPECIFIEKE GEGEVENS.
kubectl apply -f argocd-cm.yaml

------ Get admin password from the secret argocd-initial-admin-secret using the following cli-command:
kubectl get secret argocd-initial-admin-secret -n argocd -o jsonpath='{.data.password}' | base64 -d



